中文 [簡中](README-zh-Hans.md)

# Cocos Creator Specification

爲本團隊制定的cocos creator 代碼風格指導

本團隊 creator 採用 typescript 作爲開發語言 故此規範也可作爲 typescript 項目的 參考規範

1. 此規範旨在 使團隊代碼形成一致的風格 以保證項目代碼的 可讀 可維護 
2. 此規範參考了 typescript 習慣 和 creator 特殊環境 以及 本人 的經驗 制定
3. 任何規範都無法滿足全部情況 當此規範中的內容 和實際情況 以及現有習慣產生衝突時 應該 團隊協商本着以人爲本的原則 選擇 符合 人類習慣 和 可讀性 的解決方案

# 檔案/檔案夾命名

對於檔案夾應該作爲一個軟件包 來審視 將不同的內容 分包到不同 檔案夾中管理 故

1. 每個檔案夾 應該以 單個英文 單數名稱 小寫 作爲檔案夾名字
2. 名稱應該 能夠簡短的概括出 軟件包提供的功能
3. 當包含多個單詞時 應該 使用分層形式 比如 不應該使用 `zoo-animal` 而應該使用 `zoo/animal` 作爲正確的名稱

對於 typescript 檔案命名 應該遵照如下原則

1. 使用 英文 小寫 名詞 命名 
2. 對於多個 單詞組成的名稱 使用 **-** 連接

對於 creator 組件腳本 應該儘量遵循 typescript 檔案命名規則，同時 爲名稱應該加上 檔案夾 全路徑 

比如 有個 猴子的ai腳本 位於 檔案夾 /monster/ai 下面 則應該以 monster-ai-monkey 作爲腳本檔案名 其全路徑則爲 `/monster/ai/monster-ai-monkey`

> 之所以採用如此冗餘的命名前綴 是爲了保證 檔案名的 唯一，creator 設計上的不足導致 動態創建組件時 是依據的檔案名 作爲腳本識別名稱 故若有多個同名腳本(即使位於不同檔案夾下) 無法確定動態創建的 腳本究竟是哪個

# 代碼命名

使用 [駝峰式大小寫](https://zh.wikipedia.org/wiki/%E9%A7%9D%E5%B3%B0%E5%BC%8F%E5%A4%A7%E5%B0%8F%E5%AF%AB) 作爲主體命名原則

## 型別命名

1. 所有 型別 應該以 首字母 大寫形式 命名 比如 Animal HashMap
2. 所有 接口 應該 在 型別命名的基礎上 在最前端加上 I 代表是一個接口 比如 IWriter IMap
3. 枚舉的 值 也應該 按照 型別命名 的規則命名 比如 `enum Week { Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday }`

## 常量與變量

1. 任何情況都不應該使用 過時的 `var` 定義任何東西
2. 常量應該全部以 `const` 定義 常量命名應該按照 型別命名規則進行命名
3. 對於只讀變量使用 `const` 定義 可寫變量使用 `let` 定義
4. 變量 使用 首字母 小寫的 形式 命名 比如 firstName lastName
5. 應該使用 英文名詞 取名
6. 現代編譯器都提供了完善的代碼提示和自動補齊功能 故 不要使用 簡寫命名 而應該使用 名詞全稱作爲名字(除非這一簡寫 已經形成了顯而易見的 習慣 比如 使用 i 作爲計數器)
7. 當名詞全稱 和 語言關鍵字 等發送衝突時 應該使用 習慣上的 簡寫 比如 (error 簡寫爲 err,  event 簡寫爲 evt)

## 單件

所有 單件 應該 僅提供 get instance 的 實例獲取方法 

並且 單件型別 應該 以 Service 作爲 後綴，檔案名 應該以 -service 結尾 

比如 有一個 LogService 單件應該 存儲在 log-service 檔案中 並按下面方式提供

```
class LogService {
    private constructor() { }
    private static instance_: LogService
    static get instance(): LogService {
        if (!LogService.instance_) {
            LogService.instance_ = new LogService()
        }
        return LogService.instance_
    }
}
```

## 成員屬性

1. 對於 成員屬性 應該使用和 變量命名相同的規則
2. 對於 私有成員 屬性 應該在名稱後面 加上 **_**

```
class Cat {
    firstName: string
    protected lastName: string
    private id_: number // 私有屬性 使用 _ 後綴
}
```

## 方法

1. 方法名稱 應該使用 首字母 小寫的 形式 命名
2. 方法應該 儘量選用動詞 命名
3. class 的 私有 方法 應該以 **_** 作爲前綴

```
// 應該在方法中中 選用 英文動詞
function catSpeak() {
    console.log(`喵~`)
}

class Animal {
    speak() {
        console.log(`animal speak`)
    }
}
class Dog extends Animal {
    speak() {
        super.speak()
        this._speak()
    }
    // 私有方法應該以 _ 作爲前綴
    private _speak() {
        console.log(`汪~`)
    }
}
```