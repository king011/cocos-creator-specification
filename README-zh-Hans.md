[中文](README.md) 簡中

# Cocos Creator Specification

为本团队制定的cocos creator 代码风格指导

本团队 creator 采用 typescript 作为开发语言 故此规范也可作为 typescript 项目的 参考规范

1. 此规范旨在 使团队代码形成一致的风格 以保证项目代码的 可读 可维护 
2. 此规范参考了 typescript 习惯 和 creator 特殊环境 以及 本人 的经验 制定
3. 任何规范都无法满足全部情况 当此规范中的内容 和实际情况 以及现有习惯产生冲突时 应该 团队协商本着以人为本的原则 选择 符合 人类习惯 和 可读性 的解决方案

# 档案/档案夹命名

对于档案夹应该作为一个软件包 来审视 将不同的内容 分包到不同 档案夹中管理 故

1. 每个档案夹 应该以 单个英文 单数名称 小写 作为档案夹名字
2. 名称应该 能够简短的概括出 软件包提供的功能
3. 当包含多个单词时 应该 使用分层形式 比如 不应该使用 `zoo-animal` 而应该使用 `zoo/animal` 作为正确的名称

对于 typescript 档案命名 应该遵照如下原则

1. 使用 英文 小写 名词 命名 
2. 对于多个 单词组成的名称 使用 **-** 连接

对于 creator 组件脚本 应该尽量遵循 typescript 档案命名规则，同时 为名称应该加上 档案夹 全路径 

比如 有个 猴子的ai脚本 位于 档案夹 /monster/ai 下面 则应该以 monster-ai-monkey 作为脚本档案名 其全路径则为 `/monster/ai/monster-ai-monkey`

> 之所以采用如此冗余的命名前缀 是为了保证 档案名的 唯一，creator 设计上的不足导致 动态创建组件时 是依据的档案名 作为脚本识别名称 故若有多个同名脚本(即使位于不同档案夹下) 无法确定动态创建的 脚本究竟是哪个

# 代码命名

使用 [驼峰式大小写](https://zh.wikipedia.org/wiki/%E9%A7%9D%E5%B3%B0%E5%BC%8F%E5%A4%A7%E5%B0%8F%E5%AF%AB) 作为主体命名原则

## 型别命名

1. 所有 型别 应该以 首字母 大写形式 命名 比如 Animal HashMap
2. 所有 接口 应该 在 型别命名的基础上 在最前端加上 I 代表是一个接口 比如 IWriter IMap
3. 枚举的 值 也应该 按照 型别命名 的规则命名 比如 `enum Week { Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday }`

## 常量与变量

1. 任何情况都不应该使用 过时的 `var` 定义任何东西
2. 常量应该全部以 `const` 定义 常量命名应该按照 型别命名规则进行命名
3. 对于只读变量使用 `const` 定义 可写变量使用 `let` 定义
4. 变量 使用 首字母 小写的 形式 命名 比如 firstName lastName
5. 应该使用 英文名词 取名
6. 现代编译器都提供了完善的代码提示和自动补齐功能 故 不要使用 简写命名 而应该使用 名词全称作为名字(除非这一简写 已经形成了显而易见的 习惯 比如 使用 i 作为计数器)
7. 当名词全称 和 语言关键字 等发送冲突时 应该使用 习惯上的 简写 比如 (error 简写为 err,  event 简写为 evt)

## 单件

所有 单件 应该 仅提供 get instance 的 实例获取方法 

并且 单件型别 应该 以 Service 作为 后缀，档案名 应该以 -service 结尾 

比如 有一个 LogService 单件应该 存储在 log-service 档案中 并按下面方式提供

```
class LogService {
    private constructor() { }
    private static instance_: LogService
    static get instance(): LogService {
        if (!LogService.instance_) {
            LogService.instance_ = new LogService()
        }
        return LogService.instance_
    }
}
```

## 成员属性

1. 对于 成员属性 应该使用和 变量命名相同的规则
2. 对于 私有成员 属性 应该在名称后面 加上 **_**

```
class Cat {
    firstName: string
    protected lastName: string
    private id_: number // 私有属性 使用 _ 后缀
}
```

## 方法

1. 方法名称 应该使用 首字母 小写的 形式 命名
2. 方法应该 尽量选用动词 命名
3. class 的 私有 方法 应该以 **_** 作为前缀

```
// 应该在方法中中 选用 英文动词
function catSpeak() {
    console.log(`喵~`)
}

class Animal {
    speak() {
        console.log(`animal speak`)
    }
}
class Dog extends Animal {
    speak() {
        super.speak()
        this._speak()
    }
    // 私有方法应该以 _ 作为前缀
    private _speak() {
        console.log(`汪~`)
    }
}
```